# Ruurd Movie Maker
Video editor created with Electron

**How to use**

Put [ffmpeg.exe](https://www.ffmpeg.org/download.html) in resources folder after isntalling exe from releases. To upload to youtube put cliend_id.json in the resources folder. [Here's a page about client_id.json](https://developers.google.com/youtube/registering_an_application)

**Features**
* Cut video
    * Set start point
    * Set end point
    * Split video
* Change video volume
* Change playback speed
* Concatenate videos

**Screenshot**

![Screenshot of the current version](screenshots/main.png?raw=true "Pre Alpha Screenshot")

**Todo**

See [Todo.md](Todo.md)
